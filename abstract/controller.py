from flask import request, render_template
from logic.errors import BuildControlError, ArgsTypeNotValidError, JsonNotValidError


class Controller:
    def __init__(self, db, app):
        self.db = db
        self.app = app

    def route(self, fn_name, **kwargs):
        rule = kwargs.pop('rule', None)

        if not hasattr(self, fn_name):
            raise BuildControlError('Post_route "{}" function not found!'.format(fn_name))
        elif rule is None:
            rule = '/' + fn_name

        fn = getattr(self, fn_name)
        if 'methods' not in kwargs:
            kwargs['methods'] = ['POST']

        fn = self.app.route(rule, **kwargs)(fn)
        setattr(self, fn_name, fn)

    def render_template(self, template_name_or_list, **context):
        context['generic_key'] = self.app.get_generic_key()
        return render_template(template_name_or_list, **context)

    @staticmethod
    def parse_request_json(args: list, def_args: list = None):
        """
        Функция для вытаскивание из request json'а данных
        :param args: список полей которые надо достать
        Обязательное поле приводит к исключению JsonNotValidError при отсутствии в словаре
        :param def_args: список необязательных полей которые надо достать
        Необязательное поле возвращяет None при отсутствии в словаре
        :return: tuple значение полей по порядку
        Пример объекта args или def_args:
        [
            (<str: Имя параметра>, <type: Тип параметра к которому он будет преобразован>),
            ('Message', str),
            ('ToClient', bool),
            ...
        ]
        """
        if not all(
                (type(x) is tuple) and
                (len(x) == 2) and
                (type(x[0]) is str) and
                (type(x[1]) is type)
                for x in args
        ):
            raise ArgsTypeNotValidError()

        # берём json
        data = request.get_json(silent=True)

        # парсим обязательные параметры
        try:
            result = [x[1](data[x[0]]) if data[x[0]] is not None else None for x in args]
        except (ValueError, KeyError):
            raise JsonNotValidError('JSON:' + str(data))

        # парсим не обязательные параметры
        if def_args is not None:
            if not all(
                    (type(x) is tuple) and
                    (len(x) == 2) and
                    (type(x[0]) is str) and
                    (type(x[1]) is type)
                    for x in def_args
            ):
                raise ArgsTypeNotValidError()

            result += [
                x[1](data[x[0]])
                if (x[0] in data) and (data[x[0]] is not None)
                else None
                for x in def_args
            ]

        # возвращяем Tuple
        return tuple(result)

    def build_methods(self):
        raise NotImplementedError()
