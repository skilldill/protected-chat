from abstract.controller import Controller
from logic.errors import *


class User:
    def __init__(self, uid, name):
        self.uid = uid
        self.name = name

    def __hash__(self):
        return hash((
            hash(self.uid),
            hash(self.name)
        ))

    def __eq__(self, other):
        return hash(self) == hash(other)

    def __ne__(self, other):
        return not (self == other)

    def __str__(self):
        return '{}[{}]'.format(self.name, self.uid)

    def __repr__(self):
        return str(self)

    def get_json(self):
        return {
            'id': self.uid,
            'name': self.name
        }


class UserController(Controller):
    def __init__(self, db, app):
        Controller.__init__(self, db, app)
        self.users = {}
        self.password_hashes = {}
        self.load_users()

    def load_users(self):
        print('Run load users')
        data = self.db.send_query('get_users')
        if data is None:
            print('Loading users failed!')

        for udata in data:
            user = self.add_user(*udata)
            print('Load', user, self.password_hashes[user])

    def create_user(self, name, pass_hash):
        uid = self.db.send_query('add_user', (name, pass_hash))
        print('uid:', uid)
        self.add_user(uid, name, pass_hash)

    def add_user(self, uid, name, pass_hash):
        user = User(uid, name)
        self.users[uid] = user
        self.password_hashes[user] = pass_hash
        return user

    def exist_username(self, name):
        return self.get_user_by_name(name) is not None

    def get_user_by_id(self, uid):
        return self.users.get(uid, None)

    def get_user_by_name(self, name):
        for u in self.users.values():
            if u.name == name:
                return u
        return None

    def get_current_user(self, is_raise: bool=True):
        token = self.app.get_current_token()
        data = self.db.send_query('get_user_id_on_token', (token,))

        if data is None or len(data) == 0:
            if is_raise:
                raise TokenError('Token not valid!')
            else:
                return None

        return self.get_user_by_id(data[0][0])

    # ----------------------------------
    # --------- BUILDS METHODS ---------
    # ----------------------------------

    def build_methods(self):
        self.route('registration')
        self.route('login')
        self.route('logout', is_auth=True)
        self.route('find_user_by_name', is_auth=True)

    def registration(self):
        name, pass_hash = self.parse_request_json(
            [
                ('name', str),
                ('pass_hash', str)
            ]
        )
        name = name.strip()
        pass_hash = pass_hash.strip()

        if (name == '') or (pass_hash == '') or self.get_user_by_name(name) is not None:
            return {'status': 'FAILED'}

        self.create_user(name, pass_hash)

        return {'status': 'OK'}

    def login(self):
        name, pass_hash = self.parse_request_json(
            [
                ('name', str),
                ('pass_hash', str)
            ]
        )
        name = name.strip()
        pass_hash = pass_hash.strip()

        if (name == '') or (pass_hash == '') or (self.get_current_user(False) is not None):
            print('Login failed 1')
            return {'status': 'FAILED'}

        user = self.get_user_by_name(name)

        if user is None or self.password_hashes[user] != pass_hash:
            print('Login failed 2')
            return {'status': 'FAILED'}

        token = self.app.get_current_token()
        self.db.send_query('add_token', (user.uid, token))

        print('Login success')
        return {'status': 'OK'}

    def logout(self):
        user = self.get_current_user(False)
        if user is None:
            return {'status': 'FAILED'}

        token = self.app.get_current_token()
        self.db.send_query('remove_token', (token,))
        return {'status': 'OK'}

    def find_user_by_name(self):
        name, = self.parse_request_json([('name', str)])
        name = name.strip()

        if name == '':
            return []

        find_names = []

        for user in self.users.values():
            if name in user.name:
                find_names.append(user.get_json())

        return find_names
