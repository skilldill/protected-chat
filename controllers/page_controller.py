from abstract.controller import Controller


class PageController(Controller):
    def __init__(self, db, app):
        Controller.__init__(self, db, app)

    # ----------------------------------
    # --------- BUILDS METHODS ---------
    # ----------------------------------

    def build_methods(self):
        self.route('main_page', rule='/', methods=['GET'])

    def main_page(self):
        user = self.app.user_controller.get_current_user(False)
        print('User:', user)
        if user is None:
            return self.render_template('main.html')
        else:
            return self.render_template('room.html', user=user)
