from abstract.controller import Controller
from logic.errors import UserNoAccessToRoomError
from datetime import datetime


class Message:
    def __init__(self, uid, room_id, owner_id, message, date, app):
        self.uid = uid
        self.room_id = room_id
        self.owner_id = owner_id
        self.message = message
        self.date = date
        self.app = app

    def get_json(self):
        owner = self.app.user_controller.get_user_by_id(self.owner_id)
        return {
            'id': self.uid,
            'room_id': self.room_id,
            'owner': owner.get_json(),
            'message': self.message,
            'date': self.date
        }


class Room:
    def __init__(self, uid, owner_id, name, db, app):
        self.uid = uid
        self.owner_id = owner_id
        self.name = name
        self.db = db
        self.app = app

    def get_messages(self, limit, offset):
        data = self.db.send_query('get_messages', (self.uid, limit, offset))
        return [Message(*data_message, self.app).get_json() for data_message in data]

    def get_member_ids(self):
        data = self.db.send_query('get_room_members', (self.uid,))
        return [x[0] for x in data]

    def get_json(self):
        owner = self.app.user_controller.get_user_by_id(self.owner_id)
        return {
            'id': self.uid,
            'name': self.name,
            'owner': owner.get_json()
        }


class RoomController(Controller):
    def __init__(self, db, app):
        Controller.__init__(self, db, app)
        self.rooms = {}

        self.load_rooms()

    def load_rooms(self):
        print('Run loading rooms')
        for room_data in self.db.send_query('get_rooms'):
            room = self.add_room(*room_data)
            print('Load "{}" room with {} members'.format(room.name, len(room.get_member_ids())))

    def add_room(self, uid, owner_id, name):
        room = Room(uid, owner_id, name, self.db, self.app)
        self.rooms[uid] = room
        return room

    def get_room(self, room_id):
        return self.rooms[room_id]

    def check_user_access(self, room_id, user_id, is_raise=True):
        data = self.db.send_query('check_user_access', (room_id, user_id))

        is_access = data is not None and len(data) > 0

        if not is_raise:
            return is_access

        if not is_access:
            raise UserNoAccessToRoomError()

    def remove_room(self, room_id):
        self.db.send_query('remove_room_messages', (room_id,))
        self.db.send_query('remove_room', (room_id,))
        del self.rooms[room_id]

    def add_room_member(self, room_id, user_id):
        self.db.send_query('add_room_member', (room_id, user_id))

    # ----------------------------------
    # --------- BUILDS METHODS ---------
    # ----------------------------------

    def build_methods(self):
        self.route('create_room', is_auth=True)
        self.route('get_rooms', is_auth=True)
        self.route('get_room_members', is_auth=True)
        self.route('invite_user', is_auth=True)
        self.route('leave_the_room', is_auth=True)

        self.route('send_message', is_auth=True)
        self.route('get_messages', is_auth=True)

    def create_room(self):
        name, = self.parse_request_json([('name', str)])
        user = self.app.user_controller.get_current_user()

        uid = self.db.send_query('create_room', (user.uid, name))
        print(name)
        if uid is None:
            return {'status': 'FAILED'}

        self.add_room_member(uid, user.uid)
        self.add_room(uid, user.uid, name)
        return {'status': 'OK'}

    def get_rooms(self):
        user = self.app.user_controller.get_current_user()
        room_ids = [x[0] for x in self.db.send_query('get_user_rooms', (user.uid,))]
        data = [self.get_room(x).get_json() for x in room_ids]
        return data

    def leave_the_room(self):
        room_id, = self.parse_request_json([('room_id', int)])
        user = self.app.user_controller.get_current_user()
        self.check_user_access(room_id, user.uid)

        # remove member
        self.db.send_query('remove_room_member', (room_id, user.uid))
        room = self.get_room(room_id)

        if len(room.get_member_ids()) == 0:
            # remove room if count members is 0
            self.remove_room(room_id)

        return {'status': 'OK'}

    def get_room_members(self):
        room_id, = self.parse_request_json([('room_id', int)])
        user = self.app.user_controller.get_current_user()
        self.check_user_access(room_id, user.uid)

        room = self.get_room(room_id)
        members = []

        for member_id in room.get_member_ids():
            user = self.app.user_controller.get_user_by_id(member_id)
            members.append(user.get_json())

        return members

    def invite_user(self):
        inv_user_id, room_id = self.parse_request_json([
            ('user_id', int),
            ('room_id', int)
        ])

        user = self.app.user_controller.get_current_user()
        self.check_user_access(room_id, user.uid)

        if self.check_user_access(room_id, inv_user_id, False):
            return {'status': 'FAILED'}

        self.add_room_member(room_id, inv_user_id)
        return {'status': 'OK'}

    def send_message(self):
        message, room_id = self.parse_request_json([
            ('message', str),
            ('room_id', int)
        ])

        user = self.app.user_controller.get_current_user()
        self.check_user_access(room_id, user.uid)

        tx_date = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        self.db.send_query('add_message', (room_id, user.uid, message, tx_date))

        return {'status': 'OK'}

    def get_messages(self):
        room_id, limit, offset = self.parse_request_json([
            ('room_id', int),
            ('limit', int),
            ('offset', int)
        ])

        user = self.app.user_controller.get_current_user()
        self.check_user_access(room_id, user.uid)
        room = self.get_room(room_id)

        return room.get_messages(limit, offset)
