FROM ubuntu:latest
MAINTAINER "klik.9696@mail.ru"
RUN apt-get update -y
RUN apt-get install -y python3-pip python3-dev build-essential
COPY . /app
WORKDIR /app
ENV PYTHONPATH /app:$PYTHONPATH
RUN pip3 install -r requirements.txt
ENTRYPOINT ["python3"]
CMD ["server.py"]