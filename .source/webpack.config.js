module.exports={
    output:{
        filename: 'bundle.js'
    },

    resolve: {
      extensions: [".js", ".jsx", ".js", ".json"]
    },
    module: {
        rules: [
          {
            test: /\.(js|jsx)$/,
            exclude: /(node_modules)/,
            loader: 'babel-loader',
            query: {
              presets: ["@babel/preset-env", "@babel/preset-react"],
            },
          },
        ],
    },
}