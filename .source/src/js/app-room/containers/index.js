import { connect } from 'react-redux'

import ListRooms from '../components/ListRooms'
import SendBlock from '../components/SendBlock'
import ChatHeader from '../components/ChatHeader'
import MessageBlock from '../components/MessagesBlock'
import RoomCreater from '../components/RoomCreater'
import KeyCreater from '../components/KeyCreater'

export const ListRoomsContainer=connect((state)=>({ rooms:state.rooms }))(ListRooms)
export const SendBlockContainer=connect((state)=>({ message:state.message, roomId:state.room.id }))(SendBlock)
export const ChatHeaderContainer=connect((state)=>({ room:state.room, friend:state.friend, friends:state.friends }))(ChatHeader)
export const MessageBlockContainer=connect((state)=>({ messages:state.messages, roomId:state.room.id }))(MessageBlock)
export const RoomCreaterContainer=connect((state)=>({ show:state.showSettingsRoom, nameNewRoom:state.nameNewRoom }))(RoomCreater)
export const KeyCreaterContainer=connect((state)=>({ show:state.showSettingsKey, 
                                                     secretKey:state.secretKey, 
                                                     room:state.room,
                                                     isValidKey:state.isValidSecretKey }))(KeyCreater)