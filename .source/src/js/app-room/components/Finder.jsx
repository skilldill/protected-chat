import React from 'react'
import * as actions from '../actions'
import { runInThisContext } from 'vm';

export default class Finde extends React.Component{
    constructor(props){
        super(props)
        this.input=React.createRef()
    }


    fetchUsers(query){
        fetch('/find_user_by_name',{
            method:"post",
            headers:{
                'content-type':'application/json',
                'generic-key':localStorage.getItem('generic_key')
            },
            body:JSON.stringify({
                "name":query,
            })
        })
        .then(response=>{
            return response.json()
        })
        .then(data=>{
            console.log(data)
            let users=[]
            data.map(user=>{
                users.push(user)
            })
            this.props.dispatch(actions.setFriends(data))
        })
    }

    chooseFriend(id){
        this.props.dispatch(actions.setFriend(id))
        this.props.dispatch(actions.clearFriends())
    }

    addFriend(e){
        e.preventDefault()
        fetch('/invite_user',{
            method:"post",
            headers:{
                'content-type':'application/json',
                'generic-key':localStorage.getItem('generic_key')
            },
            body:JSON.stringify({
                "user_id":this.props.friend.id,
                "room_id":this.props.roomId
            })
        })
        .then(response=>{
            return response.json()
        })
        .then(data=>{
            console.log(data)
            this.props.dispatch(actions.clearFriend())
            this.input.current.value=""
        })
    }

    onChange(e){
        this.fetchUsers(e.target.value)
    }

    render(){
        return (
            <div className="finder" style={ { display: this.props.roomId==undefined ? "none" : "block" } }>
                <input ref={ this.input } type="text" className="finder__input" onChange={ (e)=>{ this.onChange(e) } }/>
                <ul className="finder__users">
                    {
                        this.props.friends.map((friend, id)=>(
                            <li key={ id } className="finder__user" onClick={ ()=>{ this.chooseFriend(id) } }>{ friend.name }</li>
                        ))
                    }
                </ul>
                <a href="#" className="finder__button" onClick={ (e)=>{ this.addFriend(e) } }>Add user</a>
                <span>{ this.props.friend.name }</span>
            </div>
        )
        
    }
}