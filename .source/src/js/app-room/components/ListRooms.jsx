import React from 'react'
import * as actions from '../actions'

export default class ListRooms extends React.Component{

    componentWillMount(){
        fetch('/get_rooms',{
            method:"post",
            headers:{
                'content-type':'application/json',
                'generic-key':localStorage.getItem('generic_key')
            }
        })
        .then(response=>{
            return response.json()
        })
        .then(data=>{
            var rooms=[]
            data.map(room=>{
                rooms.push(room)
            })
            this.props.dispatch(actions.setListRooms(rooms))
        })
    }

    componentDidUpdate(){
        setTimeout(()=>{
            this.props.dispatch(actions.updateRooms())
        },6000)
    }

    chooseRoom(key){
        this.props.dispatch(actions.chooseRoom(key))
    }

    createRoom(){
        this.props.dispatch(actions.showSettingsRoom(true))
    }

    render(){
        return (
            <ul className="rooms">
                <li className="rooms__item item-add" onClick={ ()=>{ this.createRoom() } }>
                    <span><i class="fas fa-plus"></i> Create new room</span>
                </li>
                {
                    this.props.rooms.map((room, id)=>(
                        <li key={ id } className="rooms__item item-room" onClick={ ()=>{ this.chooseRoom(id) } }>{ room.name }</li>
                    ))
                }
            </ul>
        )
    }
}