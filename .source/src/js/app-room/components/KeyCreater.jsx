import React from 'react'
import * as actions from '../actions'
import * as md5 from 'js-md5'

export default class KeyCreater extends React.Component{
    componentDidUpdate(){
        document.getElementById('settings-key').style.display=this.props.show ? "block" : "none"
    }

    createKey(e){
        if(this.validateKey()){
            localStorage.setItem(this.props.room.id.toString(), md5(this.props.secretKey))
            this.props.dispatch(actions.updateMessages(this.props.room.id))
            this.close()
        }
    }

    onChange(e){
        this.props.dispatch(actions.setSecretKey(e.target.value))
    }

    close(){
        this.props.dispatch(actions.showSettingsKey(false))
    }

    validateKey(){
        if(RegExp(/.{6,}/).test(this.props.secretKey))
            return true
        else    
            return false
    }

    render(){
        return (
            <div className="settings__key">
                <div className="settings__row row-up">
                    <span className="close" onClick={ ()=>{ this.close() } }>close</span>
                </div>
                <div className="settings__row">
                    <input type="text" className="settings__input" placeholder="secret key" onChange={ (e)=>{ this.onChange(e) } } value={ this.props.secretKey } />
                </div>
                <div className="settings__row">
                    <input type="button" className="settings__button" value="Create key" onClick={ ()=>{ this.createKey() } } />
                </div>
            </div>
        )
    }
}