import React from 'react'
import * as actions from '../actions'

export default class RoomCreater extends React.Component{

    close(){
        this.props.dispatch(actions.showSettingsRoom(false))
    }

    createRoom(){
        fetch('/create_room',{
            method:"post",
            headers:{
                'content-type':'application/json',
                'generic-key':localStorage.getItem('generic_key')
            },
            body:JSON.stringify({ "name":this.props.nameNewRoom })
        })
        .then(response=>{
            this.props.dispatch(actions.setNameOfNewRoom(""))
            this.props.dispatch(actions.showSettingsRoom(false))
            this.props.dispatch(actions.updateRooms())
            
        })
    }

    onChange(e){
        this.props.dispatch(actions.setNameOfNewRoom(e.target.value))
    }

    componentDidUpdate(){
        document.getElementById('settings-room').style.display=this.props.show ? "block" : "none"
    }

    render(){
        return (
            <div className="settings__room">
                <div className="settings__row row-up">
                    <span className="close" onClick={ ()=>{ this.close() } }>close</span>
                </div>
                <div className="settings__row">
                    <input type="text" className="settings__input" placeholder="room's name" onChange={ (e)=>{ this.onChange(e) } } />
                </div>
                <div className="settings__row">
                    <input type="button" className="settings__button" value="Create room" onClick={ ()=>{ this.createRoom() } } />
                </div>
            </div>
        )
    }
}