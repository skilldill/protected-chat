import React from 'react'
import * as actions from '../actions'

class Message extends React.Component{
    render(){
        const Cryptr=require('cryptr')
        const cryptr = new Cryptr(localStorage.getItem(this.props.roomId.toString())!==null ? localStorage.getItem(this.props.roomId.toString()) : "aabbccdd")

        return (
            <div key={ this.props.id } className="messages__row">
                <div className="message">
                    <div className="message__header">
                        <h4 className="message__owner">{ this.props.message.owner.name }</h4>
                        <span className="message__time">{ this.props.message.date }</span>
                    </div>
                    <p className="message__text">{ cryptr.decrypt(this.props.message.message) }</p>
                </div>
            </div>
        )
    }
}

export default class MessagesBlock extends React.Component{
    componentDidUpdate(){
        this.props.dispatch(actions.deleteTimer())
        this.props.dispatch(actions.createTimer(setTimeout(()=>{ this.props.dispatch(actions.updateMessages(this.props.roomId)) },3000)))
    }

    render(){
        return (
            <div className="messages">
                {
                    this.props.messages.reverse().map((message, id)=>(
                        <Message id={ id } message={ message } roomId={ this.props.roomId }/>  
                    ))
                }
            </div>
        )
    }
}