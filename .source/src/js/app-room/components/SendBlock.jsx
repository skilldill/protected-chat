import React from 'react'
import * as actions from '../actions'

export default class SendBlock extends React.Component{

    onChange(e){
        console.log(e.target.value)
        this.props.dispatch(actions.changeMessage(e.target.value))
    }

    send(){
        const Cryptr=require('cryptr')
        const cryptr = new Cryptr(localStorage.getItem(this.props.roomId.toString()))

        fetch('/send_message',{
            method:"post",
            headers:{
                'content-type':'application/json',
                'generic-key':localStorage.getItem('generic_key')
            },
            body:JSON.stringify({
                "room_id":this.props.roomId,
                "message":cryptr.encrypt(this.props.message) 
            })
        })
        .then(response=>{
            return response.json()
        })
        .then(data=>{
            console.log(data)
            this.props.dispatch(actions.updateMessages(this.props.roomId))
            this.props.dispatch(actions.changeMessage(""))
        })
    }    

    render(){
        return (
            <div className="send">
                <input type="text" className="send__text" onChange={ (e)=>{ this.onChange(e) } } value={ this.props.message } />
                <a href="#" className="send__btn" onClick={ ()=>{ this.send() } }>Send</a>
            </div>
        )
    }
}