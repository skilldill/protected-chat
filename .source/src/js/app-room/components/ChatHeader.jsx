import React from 'react'
import * as actions from '../actions'
import Finder from './Finder'

export default class ChatHeader extends React.Component{

    componentDidMount(){
        const keyElement=document.getElementById('key')
        const key=keyElement.getAttribute('data-key')
        localStorage.setItem('generic_key', key)
        document.getElementById('key').parentNode.removeChild(document.getElementById('key'))
    }

    logOut(){
        fetch('/logout',{
            method:"post",
            headers:{
                'content-type':'application/json',
                'generic-key':localStorage.getItem('generic_key')
            }
        })
        .then(response=>{
            location.reload()
        })
    }

    componentDidUpdate(){
        fetch('/get_messages',{
            method:"post",
            headers:{
                'content-type':'application/json',
                'generic-key':localStorage.getItem('generic_key')
            },
            body:JSON.stringify({
                "room_id":this.props.room.id,
                "limit":20,
                "offset":0
            })
        })
        .then(response=>{
            return response.json()
        })
        .then(data=>{
            console.log(data)
            var messages=[]
            data.map(message=>{
                messages.push(message)
            })
            console.log(messages)
            this.props.dispatch(actions.setMessages(messages))

            if(localStorage.getItem(this.props.room.id.toString())==null)
                this.props.dispatch(actions.showSettingsKey(true))
        })
    }

    render(){
        return (
            <div className="header">
                <div className="header__cell cell-main">
                    <div className="header__row">
                        <h2 className="header__title">{ this.props.room.name }</h2>
                    </div>
                    <div className="header__row">
                        <Finder 
                            friends={ this.props.friends }
                            friend={ this.props.friend } 
                            dispatch={ this.props.dispatch }
                            roomId={ this.props.room.id } 
                        />
                    </div>
                </div>
                <div className="header__cell cell-logout" onClick={ ()=>{ this.logOut() } }>
                    <span><i class="fas fa-sign-out-alt"></i>  Logout</span>
                </div>
            </div>
        )
    }
}