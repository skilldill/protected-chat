import * as actions from '../actions'

export const initialState={
    rooms:[],
    room:{},
    nameNewRoom:"",//for create new room

    messages:[],
    message:"",

    secretKey:"",

    showSettingsRoom:false,
    showSettingsKey:false,

    friend:{},
    friends:[],

    timer:0
}

export const reducer=(state=initialState, action)=>{
    switch(action.type){

        case actions.SET_LIST_ROOMS:
            return { ...state, ...{ rooms:action.data } }

        case actions.SET_NAME_OF_NEW_ROOM:
            return { ...state, ...{ nameNewRoom:action.data } }

        case actions.CHOOSE_ROOM:
            return { ...state, ...{ room:state.rooms[action.data] } }

        case actions.SET_MESSAGES:
            return { ...state, ...{ messages: action.data } }

        case actions.CHANGE_MESSAGE:
            return { ...state, ...{ message: action.data } }

        case actions.SHOW_SETTINGS_ROOM:
            return { ...state, ...{ showSettingsRoom:action.data } }
        
        case actions.SHOW_SETTINGS_KEY:
            return { ...state, ...{ showSettingsKey:action.data } }

        case actions.SET_SECRET_KEY:
            return { ...state, ...{ secretKey:action.data } }

        case actions.SET_FRIEND:
            return { ...state, ...{ friend:state.friends[action.data] } }

        case actions.SET_FRIENDS:
            return { ...state, ...{ friends:action.data } }

        case actions.CLEAR_FRIENDS:
            return { ...state, ...{ friends:[] } }
        
        case actions.CLEAR_FRIEND:
            return { ...state, ...{ friend:{} } }

        case actions.CREATE_TIMER:
            return { ...state, ...{ timer:action.data } }
        
        case actions.DELETE_TIMER:
            return { ...state, ...{ timer:clearTimeout(state.timer) } }

        default:
            return state
    }
}