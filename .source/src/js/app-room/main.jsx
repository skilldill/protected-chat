import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { ListRoomsContainer, SendBlockContainer, ChatHeaderContainer, 
         MessageBlockContainer, RoomCreaterContainer, KeyCreaterContainer } from './containers'
import { storeFactory } from './store'

const store=storeFactory()

render(
    <Provider store={ store }>
        <ListRoomsContainer />
    </Provider>,
    document.getElementById('rooms')
)

render(
    <Provider store={ store }>
        <SendBlockContainer />
    </Provider>,
    document.getElementById('send')
)

render(
    <Provider store={ store }>
        <ChatHeaderContainer />
    </Provider>,
    document.getElementById('chat-header')
)

render(
    <Provider store={ store }>
        <MessageBlockContainer />
    </Provider>,
    document.getElementById('messages')
)

render(
    <Provider store={ store }>
        <RoomCreaterContainer />
    </Provider>,
    document.getElementById('settings-room')
)

render(
    <Provider store={ store }>
        <KeyCreaterContainer />
    </Provider>,
    document.getElementById('settings-key')
)