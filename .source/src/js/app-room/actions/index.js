export const SET_LIST_ROOMS="set_list_rooms"
export const SET_MESSAGES="set_messages"
export const CHANGE_MESSAGE="change_message"
export const CHOOSE_ROOM="choose_room"
export const SHOW_SETTINGS_ROOM="show_settings_room"
export const SHOW_SETTINGS_KEY="show_settings_key"
export const SET_NAME_OF_NEW_ROOM="set_name_of_new_room"
export const SET_SECRET_KEY="set_secret_key"
export const IS_VALIDATE_KEY="is_validate_key"
export const SET_FRIEND="set_friend"
export const SET_FRIENDS="set_friends"
export const CLEAR_FRIEND="clear_friend"
export const CLEAR_FRIENDS="clear_friends"

export const CREATE_TIMER="create_timer"
export const DELETE_TIMER="delete_timer"

export const setListRooms=(rooms)=>({ type:SET_LIST_ROOMS, data:rooms })
export const setMessages=(messages)=>({ type:SET_MESSAGES, data:messages })
export const changeMessage=(message)=>({ type:CHANGE_MESSAGE, data:message })
export const chooseRoom=(key)=>({ type:CHOOSE_ROOM, data:key })
export const showSettingsRoom=(show)=>({ type:SHOW_SETTINGS_ROOM, data:show })
export const showSettingsKey=(show)=>({ type:SHOW_SETTINGS_KEY, data:show })
export const setNameOfNewRoom=(name)=>({ type:SET_NAME_OF_NEW_ROOM, data:name })
export const setSecretKey=(key)=>({ type:SET_SECRET_KEY, data:key })
export const setFriend=(id)=>({ type:SET_FRIEND, data:id })
export const setFriends=(friends)=>({ type:SET_FRIENDS, data:friends })
export const clearFriends=()=>({ type:CLEAR_FRIENDS })
export const clearFriend=()=>({ type:CLEAR_FRIEND })

export const createTimer=(timer)=>({ type:CREATE_TIMER, data:timer })
export const deleteTimer=()=>({ type:DELETE_TIMER })


//get rooms from server
export function updateRooms(){
    return dispatch=>{
        fetch('/get_rooms',
        {
            method:"post",
            headers:{
                'content-type':'application/json',
                'generic-key':localStorage.getItem('generic_key')
        }})
        .then(response=>{
            return response.json()
        })
        .then(data=>{
            var rooms=[]
            data.map(room=>{
                rooms.push(room)
            })
            dispatch(setListRooms(rooms))
        })
    }
}

//get messages from server
export function updateMessages(room_id){
    return dispatch=>{
        fetch('/get_messages',
        {
            method:"post",
            headers:{
                'content-type':'application/json',
                'generic-key':localStorage.getItem('generic_key')
            },
            body:JSON.stringify({
                "room_id":room_id,
                "limit":20,
                "offset":0
            })
        })
        .then(response=>{
            return response.json()
        })
        .then(data=>{
            var messages=[]
            data.map(message=>{
                messages.push(message)
            })
            dispatch(setMessages(messages))
        })
    }
}

