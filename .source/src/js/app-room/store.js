import { createStore, applyMiddleware } from 'redux'
import { reducer, initialState } from './reducers'

const logger=store => next => action => {
    console.log(action)
    let result=next(action)
    console.log(store.getState())
}

const actionFn = ({ dispatch, getState }) => next => (action) => {
    return action instanceof Function ? action(dispatch, getState) : next(action)
}

export const storeFactory=()=>createStore(reducer, initialState, applyMiddleware(logger, actionFn))
