import { connect } from 'react-redux'
import Autorization from '../components/Authorization'
import Registration from '../components/Registration'
import Loading from '../components/Loading'

export const AuthorizationContainer = connect((state)=>({ show:state.show_authorization,
                                                            pass:state.authorizationPass,
                                                            name:state.authorizationName,
                                                            isReady:state.authorizationIsReady,
                                                            error:state.authorizationErr }))(Autorization)

export const RegistrationContainer = connect((state)=>({ show:state.show_registration,
                                                        pass:state.registrationPass,
                                                        name:state.registrationName,
                                                        isReady:state.registrationIsReady,
                                                        error:state.registrationErr }))(Registration)

export const LoadingContainer = connect((state)=>({ show:state.show_loading, process:state.loading_process }))(Loading)
