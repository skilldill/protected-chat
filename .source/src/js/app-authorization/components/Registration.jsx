import React from 'react'
import * as actions from '../actions'
import * as md5 from 'js-md5'

export default class Registration extends React.Component{

    showLogIn(e){
        e.preventDefault()
        this.props.dispatch(actions.toggleForm())
    }

    setName(e){
        this.props.dispatch(actions.registrationSetName(e.target.value))
        if(this.props.name!=="" && this.props.pass!=="")
            this.props.dispatch(actions.registrationIsReady(true))
        else
            this.props.dispatch(actions.registrationIsReady(false))
    }

    setPass(e){
        this.props.dispatch(actions.registrationSetPass(e.target.value))
        if(this.props.name!=="" && this.props.pass!=="")
            this.props.dispatch(actions.registrationIsReady(true))
        else
            this.props.dispatch(actions.registrationIsReady(false))
    }

    submit(){
        if(this.props.isReady){
            this.props.dispatch(actions.loadingShow(true))
            this.props.dispatch(actions.loadingProcess("Please wite register..."))
            fetch("/registration",{
                method:"post",
                body:JSON.stringify({
                    name:this.props.name,
                    pass_hash:md5(this.props.pass)
                }),
                headers:{
                    'content-type':'application/json',
                    'generic-key':localStorage.getItem('generic_key')
                }
            })
            .then(response=>{
                console.log(response)
                this.props.dispatch(actions.loadingShow(false))
                this.props.dispatch(actions.loadingProcess("please wite register"))                
                console.log(response.json())
                location.reload()
            })
        }
    }

    render(){
        return(
            <div className="registration" style={ { display: this.props.show ? "block" : "none" } }>
                <h2 className="registration__title">Sign Up</h2>
                <span className="form__error">{ this.props.error }</span>
                <form action="/registration" className="registration__form" method="POST">
                    <input className="form__input" type="text" name="login" placeholder="Login" onChange={ (e)=>{ this.setName(e) } }/>
                    <input className="form__input" type="password" name="password" placeholder="Password" onChange={ (e)=>{ this.setPass(e) } }/>
                    <input className="form__input" type="password" name="password-repeat" placeholder="Repeat password" onChange={ ()=>{} } />
                    <input className="form__input input-btn" type="btn" value="Sign Up" onClick={ ()=>{ this.submit() } }/>
                </form>
                <a href="#" onClick={ (e)=>{ this.showLogIn(e) } }>Log In</a>
            </div>
        )
    }
}