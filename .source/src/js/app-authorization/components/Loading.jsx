import React from 'react'

export default class Loading extends React.Component{

    componentDidUpdate(){
        document.getElementById("loading").style.display=this.props.show ? "block" : "none"
    }

    render(){
        return (
                <div className="loading__process">
                    <h1 className="loading__text">{ this.props.process }</h1>
                </div>
        )
    }
}