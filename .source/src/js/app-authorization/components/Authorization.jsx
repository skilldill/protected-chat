import React from 'react'
import * as actions from '../actions'
import * as md5 from 'js-md5'

export default class Authorization extends React.Component{

    componentDidMount(){
        const keyElement=document.getElementById('key')
        const key=keyElement.getAttribute('data-key')
        localStorage.setItem('generic_key', key)
        document.getElementById('key').parentNode.removeChild(document.getElementById('key'))
    }

    showSignIn(e){
        e.preventDefault()
        this.props.dispatch(actions.toggleForm())
    }

    setName(e){
        this.props.dispatch(actions.authorizationSetName(e.target.value))
        if(this.props.name!=="" && this.props.pass!=="")
            this.props.dispatch(actions.authorizationIsReady(true))
        else
            this.props.dispatch(actions.authorizationIsReady(false))
    }

    setPass(e){
        this.props.dispatch(actions.authorizationSetPass(e.target.value))
        if(this.props.name!=="" && this.props.pass!=="")
            this.props.dispatch(actions.authorizationIsReady(true))
        else
            this.props.dispatch(actions.authorizationIsReady(false))
    }

    submit(){
        if(this.props.isReady){
            this.props.dispatch(actions.loadingShow(true))
            this.props.dispatch(actions.loadingProcess("Wait please login..."))
            fetch("/login",{
                method:"post",
                body:JSON.stringify({
                    name:this.props.name,
                    pass_hash:md5(this.props.pass)
                }),
                headers:{
                    'content-type':'application/json',
                    'generic-key':localStorage.getItem('generic_key')
                }
            })
            .then(response=>{
                console.log(response.json())
                this.props.dispatch(actions.loadingShow(false))
                this.props.dispatch(actions.loadingProcess(""))
                location.reload()
            })
            // then(data=>{
            //     switch(data.status){
            //         case "OK":
            //             location.reload()
            //         case "FAILED":
            //             this.props.dispatch(actions.authorizationSetErr(data.error))
            //             return
            //     }
            // })
        }
    }

    render(){
        return( 
            <div className="login" style={ { display: this.props.show ? "block" : "none" } }>
                <h2 className="login__title">Log In</h2>
                <span className="form__error">{ this.props.error }</span>
                <form action="/login" className="login__form" method="POST">
                    <input className="form__input" type="text" name="login" placeholder="Login" onChange={ (e)=>{ this.setName(e) } }/>
                    <input className="form__input" type="password" name="password" placeholder="Password" onChange={ (e)=>{ this.setPass(e) } }/>
                    <input className="form__input input-btn" type="button" value="Log In" onClick={ ()=>{ this.submit() } }/>
                </form>
                <a href="#" onClick={ (e)=>{ this.showSignIn(e) } } >Sign Up</a>
            </div>
        )
    }
}