import * as actions from '../actions'

export const initialState={
    show_registration:false,
    show_authorization:true,

    registrationIsReady:false,
    authorizationIsReady:false,
    
    registrationPass:"",
    registrationName:"",
    registrationErr:"",

    authorizationPass:"",
    authorizationName:"",
    authorizationErr:"",

    show_loading:false,
    loading_process:""
}

export function reducer(state=initialState, action){
    switch(action.type){
        case actions.TOGGLE_FORM:
            console.log(action)
            return { ...state, ...{ show_authorization: !state.show_authorization, show_registration:!state.show_registration } }

        case actions.REGISTRATION_SET_NAME:
            return { ...state, ...{ registrationName:action.data } }

        case actions.REGISTRATION_SET_PASS:
            return { ...state, ...{ registrationPass:action.data } }

        case actions.REGISTRATION_SET_ERR:
            return { ...state, ...{ registrationErr:action.data } }

        case actions.REGISTRATION_READY:
            return { ...state, ...{ registrationIsReady:action.data } }

        case actions.AUTHORIZATION_SET_NAME:
            return { ...state, ...{ authorizationName:action.data } }

        case actions.AUTHORIZATION_SET_PASS:
            return { ...state, ...{ authorizationPass:action.data } }

        case actions.AUTHORIZATION_SET_ERR:
            return { ...state, ...{ authorizationErr:action.data } }

        case actions.AUTHORIZATION_READY:
            return { ...state, ...{ authorizationIsReady:action.data } }

        case actions.LOADING_SHOW:
            return { ...state, ...{ show_loading:action.data } }

        case actions.LOADING_PROCESS:
            return { ...state, ...{ loading_process:action.data } }

        default:
            return state
    }
}