import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { storeFactory } from './store'
import { AuthorizationContainer, RegistrationContainer, LoadingContainer } from './containers'

const store=storeFactory()

class App extends React.Component{
    render(){
        return(
            <div className="authorization__forms">
                <AuthorizationContainer />
                <RegistrationContainer />
            </div>
        )
    }
}

render( 
    <Provider store={ store }>
        <App />
    </Provider>,
    document.getElementById('authorization__field')
)

render(
    <Provider store={ store }>
        <LoadingContainer />
    </Provider>,
    document.getElementById('loading')
)