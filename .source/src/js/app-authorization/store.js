import { createStore } from 'redux'
import { reducer, initialState } from './reducers'



export const storeFactory=()=>createStore(reducer, initialState)