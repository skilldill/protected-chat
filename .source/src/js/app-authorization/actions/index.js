export const TOGGLE_FORM='toggle_form'

export const REGISTRATION_READY="registration_ready"
export const REGISTRATION_SET_PASS="registration_set_pass"
export const REGISTRATION_SET_NAME="registration_set_name"
export const REGISTRATION_SET_ERR="registration_set_err"

export const AUTHORIZATION_READY="authorization_ready"
export const AUTHORIZATION_SET_PASS="authorization_set_pass"
export const AUTHORIZATION_SET_NAME="authorization_set_name"
export const AUTHORIZATION_SET_ERR="authorization_set_err"

export const LOADING_PROCESS="loading_process"
export const LOADING_SHOW="loading_show"

export const toggleForm=()=>({ type:TOGGLE_FORM })

export const registrationIsReady=(ready)=>({ type:REGISTRATION_READY, data:ready  })
export const registrationSetName=(name)=>({ type:REGISTRATION_SET_NAME, data:name })
export const registrationSetPass=(pass)=>({ type:REGISTRATION_SET_PASS, data:pass })
export const registrationSetErr=(err)=>({ type:REGISTRATION_SET_ERR, data:err })

export const authorizationIsReady=(ready)=>({ type:AUTHORIZATION_READY, data:ready })
export const authorizationSetName=(name)=>({ type:AUTHORIZATION_SET_NAME, data:name })
export const authorizationSetPass=(pass)=>({ type:AUTHORIZATION_SET_PASS, data:pass })
export const authorizationSetErr=(err)=>({ type:AUTHORIZATION_SET_ERR, data:err })

export const loadingShow=(show)=>({ type:LOADING_SHOW, data:show })
export const loadingProcess=(process)=>({ type:LOADING_PROCESS, data:process })


    


