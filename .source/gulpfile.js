const gulp=require('gulp')
const webpack=require('webpack')
const webpackStream=require('webpack-stream')
const sass = require('gulp-sass')
 
sass.compiler = require('node-sass')

gulp.task('js-auth', ()=>{
    gulp.src('./src/js/app-authorization/**/*.jsx')
    .pipe(webpackStream({
        output:{
            filename: 'bundle.js'
        },
    
        resolve: {
          extensions: [".js", ".jsx", ".js", ".json"]
        },
        module: {
            rules: [
              {
                test: /\.(js|jsx)$/,
                exclude: /(node_modules)/,
                loader: 'babel-loader',
                query: {
                  presets: ["@babel/preset-env", "@babel/preset-react"],
                },
              },
            ],
        },
    }), webpack)
    .pipe(gulp.dest('../static/js/authorization'))
})

gulp.task('js-room', ()=>{
    gulp.src('./src/js/app-room/**/*.jsx')
    .pipe(webpackStream({
        output:{
            filename: 'bundle.js'
        },
    
        resolve: {
          extensions: [".js", ".jsx", ".js", ".json"]
        },
        module: {
            rules: [
              {
                test: /\.(js|jsx)$/,
                exclude: /(node_modules)/,
                loader: 'babel-loader',
                query: {
                  presets: ["@babel/preset-env", "@babel/preset-react"],
                },
              },
            ],
        },
    }), webpack)
    .pipe(gulp.dest('../static/js/room'))
})

gulp.task('sass-auth', function () {
  return gulp.src('./src/css/authorization/style.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('../static/css/authorization'));
});

gulp.task('sass-room', function () {
    return gulp.src('./src/css/room/style.scss')
      .pipe(sass().on('error', sass.logError))
      .pipe(gulp.dest('../static/css/room'));
});

gulp.task('auth', ['js-auth', 'sass-auth'])
gulp.task('room', ['js-room', 'sass-room'])

