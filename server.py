from flask import render_template
from logic.protect_app import ProtectApp

import config
import os

app = ProtectApp(__name__)


@app.errorhandler(404)
def page_not_found():
    return render_template('/404.html'), 404


if __name__ == '__main__':
    app.config['SECRET_KEY'] = os.urandom(32).hex()
    app.run(
        host='0.0.0.0',
        port=config.PORT,
        threaded=True
    )
    app.close()
