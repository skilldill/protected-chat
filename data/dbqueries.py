
QUERIES = {
    # -------------------------- USERS --------------------------

    'get_user_id_on_token': '''
        select u.id from t_tokens as t
        join t_users as u on t.user_id = u.id
        where t.token = ?
    ''',
    'add_user': '''
        insert into t_users(name, pass_hash) values(?, ?)
    ''',
    'add_token': '''
        insert into t_tokens(user_id, token) values(?, ?)
    ''',
    'remove_token': '''
        delete from t_tokens where token = ? 
    ''',
    'get_users': '''
        select * from t_users
    ''',

    # -------------------------- ROOMS --------------------------
    'check_user_access': '''
        select * from t_room_members where room_id = ? and member_id = ?  
    ''',
    'get_rooms': '''
        select * from t_rooms
    ''',
    'get_room_members': '''
        select member_id from t_room_members where room_id = ? 
    ''',
    'get_user_rooms': '''
        select room_id from t_room_members where member_id = ? 
    ''',
    'create_room': '''
        insert into t_rooms(owner_id, name) values(?, ?)
    ''',
    'remove_room': '''
        delete from t_rooms where id = ?
    ''',

    'add_room_member': '''
        insert into t_room_members(room_id, member_id) values(?, ?)
    ''',
    'remove_room_member': '''
        delete from t_room_members where room_id = ? and member_id = ?
    ''',

    # -------------------------- MESSAGES --------------------------
    'get_messages': '''
        select * from t_messages where room_id = ? order by id desc limit ? offset ? 
    ''',
    'add_message': '''
        insert into t_messages(room_id, owner_id, message, date) values(?, ?, ?, ?)
    ''',
    'remove_room_messages': '''
        delete from t_messages where room_id = ?
    ''',
}
