from logic.errors import SQLManagerError
from queue import Queue
from threading import Thread
import sqlite3
import time
from datetime import datetime
import os


TIME_SLEEP = 0.001


class QueueWork:
    def __init__(self, query_name, args):
        self.query_name = query_name
        self.args = args
        self.is_baked = False
        self.result = None

    def wait(self):
        while not self.is_baked:
            time.sleep(TIME_SLEEP)


class SQLManager:
    """
    SQLManager class that implements the
    logic of asynchronous access to database

    Example for use

    1. Init - __init__()
    2. prepare queries - prepare_query()
    3. send queryes - send_query()
    4. closed connect - close()
    5. Enjoy =)

    """
    def __init__(self, db_file, is_create=False):
        if not is_create and not os.path.isfile(db_file):
            raise SQLManagerError('Database {} not found'.format(db_file))

        self.db_file = db_file

        self.is_work = True
        self.is_started = False
        self.query_queue = Queue()
        self.prepared_queries = {}

        self.th = Thread(target=self._main_loop)
        self.th.start()

        while not self.is_started:
            time.sleep(TIME_SLEEP)


    def __del__(self):
        self.close()

    def _get_connect(self):
        print('Connecting to {} database'.format(self.db_file))
        try:
            conn = sqlite3.connect(self.db_file)
            print('Connecting success')
        except Exception as e:
            raise SQLManagerError('Connecting to {} failed:\n{}'.format(self.db_file, str(e)))

        self.is_started = True
        return conn

    def _get_prepare_query(self, query_name):
        if query_name not in self.prepared_queries:
            raise SQLManagerError('Query {} not found'.format(query_name))

        return self.prepared_queries[query_name]

    def _main_loop(self):
        conn = self._get_connect()
        cursor = conn.cursor()
        self.is_started = True

        last_commit = datetime.now()
        send_queries = 0

        while self.is_work:
            if (datetime.now() - last_commit).seconds >= 60 and send_queries != 0:
                print('Commin [{}] query'.format(send_queries))
                conn.commit()
                send_queries = 0

            if not self.query_queue.empty():
                work = self.query_queue.get()
                try:
                    query = self._get_prepare_query(work.query_name)
                    cursor.execute(query, *work.args)
                    work.result = list(cursor.fetchall())

                    if len(work.result) == 0 and query.startswith('insert'):
                        work.result = cursor.lastrowid
                        print(work.query_name, work.result)

                except Exception as e:
                    print('=========== SQLManager EXCEPTION ===========\n{}'.format(e))
                finally:
                    work.is_baked = True
                    send_queries += 1

            time.sleep(TIME_SLEEP)

        if send_queries != 0:
            print('Commin [{}] query'.format(send_queries))
            conn.commit()

        conn.close()
        print('End query main loop')

    def prepare_query(self, query_name:str, query:str):
        """
        Method prepare query - saving query for use
        :param query_name: name the query
        :param query: text query
        """
        print('Prepared query {}'.format(query_name))
        self.prepared_queries[query_name] = query.strip()

    def send_query(self, query_name: str, *args):
        """
        Method sending prepare query with name
        :param query_name: name prepared query
        :param args: arguments for query
        :return: result query execute
        """
        work = QueueWork(query_name, args)
        self.query_queue.put(work)
        work.wait()
        return work.result

    def close(self):
        if self.is_work:
            self.is_work = False
            self.th.join()
            del self.th
