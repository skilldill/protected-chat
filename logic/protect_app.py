from flask import Flask, session

import config
from abstract.controller import Controller
from controllers.page_controller import PageController
from controllers.room_controller import RoomController
from controllers.user_controller import UserController
from data.dbqueries import QUERIES
from logic.decorators import *
from logic.errors import *
from logic.sql_manager import SQLManager


class Token:
    def __init__(self, user_id: int = -1, device_id: int = -1, token_str: str=None):
        self.user_id = user_id
        self.device_id = device_id
        self.salt = ''
        self.token_str = token_str

        if token_str is not None:
            stk = self.token_str.split('#')
            if len(stk) != 3:
                raise TokenError('Count part token is not 3')

            try:
                self.user_id = int(stk[0])
                self.device_id = int(stk[1])
                self.salt = str(stk[2])
            except Exception:
                raise TokenError('Toke format not valid')
        else:
            self.salt = os.urandom(32).hex()
            self.token_str = "{user_id}#{device_id}#{slt}".format(
                user_id=self.user_id,
                device_id=self.device_id,
                slt=self.salt
            )

    def __hash__(self):
        return hash((
            hash(self.user_id),
            hash(self.device_id),
            hash(self.salt)
        ))

    def __eq__(self, other):
        return (self.user_id, self.device_id, self.salt) == (other.user_id, other.device_id, other.salt)

    def __ne__(self, other):
        return not (self == other)

    def __str__(self):
        return self.token_str

    def __repr__(self):
        return self.token_str


class ProtectApp(Flask):
    @staticmethod
    def get_current_token(is_raise=True):
        t = request.cookies.get('token')
        if t is None or t.strip() == '':
            if is_raise:
                raise TokenIsEmptyError()
            else:
                return None
        return t

    @staticmethod
    def get_generic_key():
        if 'generic_key' not in session:
            session['generic_key'] = str(os.urandom(32).hex())
        return session['generic_key']

    def __init__(self, import_name, **kwargs):
        Flask.__init__(self, import_name, **kwargs)
        self.db = SQLManager(config.DB_FILE)
        self.prepare_queries()

        self.user_controller = UserController(self.db, self)
        self.room_controller = RoomController(self.db, self)
        self.page_controller = PageController(self.db, self)

        self.builds_controls()

    def close(self):
        if self.db is not None:
            self.db.close()
            del self.db

    def __del__(self):
        self.close()

    def builds_controls(self):
        print('======= Run build controllers =====')
        for key, value in self.__dict__.items():
            if key.endswith('_controller') and isinstance(value, Controller):
                print('Build "{}" controller'.format(key))
                value.build_methods()

    def prepare_queries(self):
        for query_name in QUERIES:
            query = QUERIES[query_name]
            self.db.prepare_query(query_name, query)

    def route(self, rule, **options):
        print('Routing "{}" method'.format(rule))
        default_value = options.pop('default_value', None)
        is_auth = options.pop('is_auth', False)

        base_rout = Flask.route(self, rule, **options)

        def wraper(fn):
            fn = wrap_func(check_generic_key, fn, self)

            if is_auth:
                fn = wrap_func(check_auth_decorator, fn, self)


            fn = wrap_func(route_exception_decorator, fn, default_value)
            fn = wrap_func(check_token_decorator, fn, self)

            fn = wrap_func(base_rout, fn)
            return fn

        return wraper
