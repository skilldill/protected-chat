
class SQLManagerError(Exception):
    pass


class TokenError(Exception):
    pass


class TokenIsEmptyError(TokenError):
    pass


class BuildControlError(Exception):
    pass


class ArgsTypeNotValidError(Exception):
    pass


class JsonNotValidError(Exception):
    pass


class UserNotAuthError(Exception):
    pass


class UserNoAccessToRoomError(Exception):
    pass


class GenericKeyAccessError(Exception):
    pass
