import os
import linecache
import sys
import traceback
from io import StringIO
import copy

import json
from datetime import datetime
from flask.wrappers import ResponseBase
from flask import Response, request, redirect, url_for

from logic.errors import *


def wrap_func(decorer, fn, *args):
    nm = fn.__name__
    fn = decorer(fn, *args)
    fn.__name__ = nm
    return fn


class ExtendedEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, str):
            return obj
        elif isinstance(obj, datetime):
            return obj.isoformat()
        return json.JSONEncoder.default(self, obj)


def check_response(rsp):
    if rsp is None:
        rsp = 'OK'

    if not isinstance(rsp, ResponseBase):
        if not isinstance(rsp, str) and request.method.upper() != 'POST':
            print('Only POST methods can send not string object')
            rsp = 'OK'

        if type(rsp) in [dict, list, object]:
            rsp = json.dumps(rsp, cls=ExtendedEncoder)
            mime_type = 'application/json'
        else:
            if not isinstance(rsp, str):
                rsp = 'OK'
            mime_type = None  # 'text/xml'

        rsp = Response(
            rsp,
            status=200,
            mimetype=mime_type
        )

    return rsp


def get_tb_next(tb):
    if tb.tb_next is None:
        return tb
    else:
        return get_tb_next(tb.tb_next)


def init_error_response(exception_msg, default_response):
    if request.method == 'GET':
        # rt = render_template('error/index.html', message=str(exception))
        rt = """
        <center style=\"position: relative;top: 50%;\">
            <font size="5" face=\"Arial\">
                Error: \"{ex}\"
            </font>
        </center>
        """.format(ex=str(exception_msg))
        return Response(rt, status=500)

    rsp = Response(
        json.dumps(
            {'error': str(exception_msg)},
            cls=ExtendedEncoder
        ),
        status=500
    ) if default_response is None else default_response

    if request.method == 'POST':
        return rsp
    else:
        # TODO: что-нибудь другое вернуть на доругой метод, наверное =^.^=
        return rsp


def check_auth_decorator(func, app):
    def wrap(*args, **kwargs):
        r = func(*args, **kwargs)
        user = app.user_controller.get_current_user(True)
        if user is None:
            raise UserNotAuthError()
        return r
    return wrap


def check_generic_key(func, app):
    def wrap(*args, **kwargs):
        if request.method == 'POST':
            generic_key = app.get_generic_key()
            print(generic_key)
            if 'generic-key' not in request.headers or request.headers['generic-key'] != generic_key:
                
                raise GenericKeyAccessError()
        r = func(*args, **kwargs)
        return r
    return wrap


def route_exception_decorator(func, _default_response):
    """Декоратор предназначеный для предотвращения ошибок в роутинге"""
    default_response = copy.copy(_default_response)

    def wrap_func(*args, **kwargs):
        try:
            rsp = func(*args, **kwargs)
            rsp = rsp
        except TokenIsEmptyError:
            rsp = redirect('/')
        except (ArgsTypeNotValidError, JsonNotValidError) as e:
            rsp = Response('Bad request:"{}"'.format(e.__class__.__name__), status=400)
        except UserNotAuthError:
            rsp = Response('User unauthorized', status=401)
        except (UserNoAccessToRoomError, GenericKeyAccessError):
            rsp = Response('No access', status=403)
        except Exception as e:
            exc_type, exc_obj, first_tb = sys.exc_info()
            tb = get_tb_next(first_tb)
            f = tb.tb_frame
            filename = f.f_code.co_filename
            linecache.checkcache(filename)
            line = linecache.getline(filename, tb.tb_lineno, f.f_globals)

            tb = StringIO()
            traceback.print_tb(first_tb, file=tb)
            msg_ex = '''
==========Exception===========
Message: {e}
Line: {ln}
Traceback:
{tb}=============================='''.format(
                e=e,
                ln=line.replace('\r', '').replace('\n', ''),
                tb=tb.getvalue()
            )
            print(msg_ex)
            rsp = init_error_response(e, default_response)
        return check_response(rsp)

    return wrap_func


def check_token_decorator(func, app):
    def wrap(*args, **kwargs):
        r = func(*args, **kwargs)
        token = app.get_current_token(False)
        if token is None:
            r.set_cookie('token', os.urandom(32).hex())
        return r
    return wrap
